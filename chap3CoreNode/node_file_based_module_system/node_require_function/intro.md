### NODE JS REQUIRE FUNCTION

La función Node.js require es la forma principal de importar un módulo al archivo actual. Hay tres tipos de
Módulos en Node.js: módulos principales, módulos de archivos y módulos de nodo externos, todos los cuales utilizan la función require.
Estamos discutiendo módulos de archivos en este momento.

Cuando hacemos una llamada requerida con una ruta relativa, por ejemplo, algo como requiera ('./ filename')
o requiera ('../ foldername / filename') - Node.js ejecuta el archivo JavaScript de destino en un nuevo ámbito y devuelve
cualquiera que sea el valor final para module.exports en ese archivo. Esta es la base de los módulos de archivos. Echemos un vistazo a la
Las ramificaciones de este diseño.



